package com.jaragon.kafkaproducer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.jaragon.kafkaproducer.kafka.producer.BookProducer;
import com.jaragon.kafkaproducer.model.Book;

@Service
public class BookService {

	@Autowired
	private BookProducer bookProducer;
	
	public Book createBook(Book book) throws JsonProcessingException {
		
		// Send to kafka
		bookProducer.sendBook(book);
		
		// Should persist data
		
		// Return
		return book;
		
	}
}
