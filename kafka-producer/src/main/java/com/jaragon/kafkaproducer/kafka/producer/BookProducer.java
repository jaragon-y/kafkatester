package com.jaragon.kafkaproducer.kafka.producer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jaragon.kafkaproducer.model.Book;

@Component
public class BookProducer {

	
	@Value("${spring.kafka.topic}")
	private String topic;
	
	private final KafkaTemplate<Integer, String> kafkaTemplate;
	private final ObjectMapper objectMapper;
	
	public BookProducer(KafkaTemplate<Integer, String> kafkaTemplate,ObjectMapper objectMapper) {
		this.kafkaTemplate = kafkaTemplate;
		this.objectMapper = objectMapper;
	}
	
	public void sendBook(Book book) throws JsonProcessingException {
		Integer key = book.getId();
		String value = objectMapper.writeValueAsString(book);
		
		kafkaTemplate.send(topic,key,value);
	
	}
}
