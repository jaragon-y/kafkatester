package com.jaragon.kafkaproducer.model;

import lombok.Data;

@Data
public class Book {
	
	private Integer id;
	
	private String name;
	
	private String author;

}
